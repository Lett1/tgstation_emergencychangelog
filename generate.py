from jinja2 import Environment, FileSystemLoader
import re
import itertools
import requests
import requests_cache
import dateutil.parser
import os, sys

# Uncomment this to cache requests
# requests_cache.install_cache('demo_cache')

cl_finder = r"\:cl\:(?P<name>.*?)(?:\r\n|\r|\n)(?P<content>.*?)\/\s*\:cl\:"

prefixes = {
    "add": "rscadd",
    "del": "rscdel",
    "tweak": "tweak",
    "balance": "balance",
    "fix": "bugfix",
    "soundadd": "soundadd",
    "sounddel": "sounddel",
    "imageadd": "imageadd",
    "imagedel": "imagedel",
    "spellcheck": "spellcheck",
    "code": "code_imp",
    "config": "config",
    "admin": "admin",
    "server": "server"
}

query = '''
{
  repository(owner: "tgstation", name: "tgstation") {
    pullRequests(first: 100, after: %s, states: MERGED, orderBy: {field: CREATED_AT, direction: DESC}) {
      edges {
        node {
          title
          body
          author {
            login
          }
          mergedAt
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
}
'''

try:
    os.environ['GITHUB_API']
except KeyError:
    print("Please set an env variable for GITHUB_API which should be a valid github api key.")
    sys.exit(1)

url = 'https://api.github.com/graphql'
token = os.environ['GITHUB_API']
headers = {'Authorization': 'token %s' % token}
endCursor = None

changes = []

while True:
    payload = query % ("null" if not endCursor else f'"{endCursor}"')
    r = requests.post(url=url, json={"query": payload}, headers=headers)

    data = r.json()

    for node in data["data"]["repository"]["pullRequests"]["edges"]:
        node = node["node"]

        cl_search = re.search(cl_finder, node["body"], flags=re.MULTILINE | re.DOTALL)

        # no CL tags found, bail out
        if not cl_search:
            continue

        content = []

        if cl_search.group("content"):
            lines = cl_search.group("content").strip().splitlines()

            # Gather individual changelog messages, each is a dict of format {tag: string, body: string}
            for line in lines:
                if not line.split(" ")[0].endswith(":"): # handle lines without tags
                    tag = None
                    body = line
                else:
                    split = line.split(":", 1)
                    tag = prefixes.get(split[0], "rscdel")
                    body = split[1]

                content.append({"tag": tag, "body": body})

        # Check if the user specified a custom name for the changelog
        if cl_search.group("name") and cl_search.group("name").strip():
            name = cl_search.group("name").strip()
        else:
            if node["author"] is not None:
                name = node["author"]["login"]
            else:
                name = "SpessmanJim" # Default fallback for @ghost

        changes.append({"content": content, "user": name, "date": dateutil.parser.parse(node["mergedAt"])})

    # Stop if we reached the end of the 
    if not data["data"]["repository"]["pullRequests"]["pageInfo"]["hasNextPage"]:
        break

    endCursor = data["data"]["repository"]["pullRequests"]["pageInfo"]["endCursor"]

# Sort by date in reverse order (newest pr's first), needed for groupby to work properly
changes.sort(key=lambda x: x["date"], reverse=True)

# Group by unique dates and materialize the dict
changes = {k: list(v) for k, v in itertools.groupby(changes, lambda x: x["date"].date())}

# Also group pr's for a given date, this merges multiple prs by the same person
for date, prs in changes.items():
    print(date)
    prs = {k: list(v) for k, v in itertools.groupby(prs, lambda x: x["user"])}
    print(prs)

file_loader = FileSystemLoader("templates")
env = Environment(loader=file_loader)

# Define output targets here, first os the template then the output file name
# Templates go in templates/, output gets written into outputs/
targets = [
    ("template.html", "changelog.html"),
    ("yaml_template.html", ".all_changelog.yml")
]

for template_file, output_file in targets:
    template = env.get_template(template_file)
    template.stream(pull_requests=changes).dump("output/"+output_file)

print ("Done")